<?php
class Imprint_Child_Init
{
	public function __construct()
	{
		global $ImprintInit;
		add_action('init', array($this, 'init'));

		// Hook into the 'wp_enqueue_scripts' action
		add_action( 'wp_enqueue_scripts', array($this, 'custom_styles'), 999);

		// Hook into the 'wp_enqueue_scripts' action for js
		//add_action( 'wp_enqueue_scripts', array($this, 'custom_scripts') );

		// this is for removals or additions after imprint has initiated
		add_action('wp_loaded', array($this, 'imprint_init_actions'), 10);

		// adding a custom action for imprint top nav, and removing the action for the previous nav

	}
	// Init Function
	function init() {
		add_editor_style();
		// Adding Featured Image Support
		register_nav_menus(array('top-navigation' => __( 'Top Navigation' )));

		// this really should be based on a setting somewhere
		//add_image_size( 'about-thumb', 108, 150, true ); // adding about thumb for sidebar

	} // end init

	// if you want to override the nav, put your nav markup here:
	function ic_top_nav() { ?>
		<nav class="navbar navbar-white header">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar first"></span>
	            <span class="icon-bar middle"></span>
	            <span class="icon-bar last"></span>
	          </button>
	          <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
				<?php if (!is_page_template('template-landing.php')): ?>
					<?php wp_nav_menu( array(
						'theme_location' => 'top-navigation',
						'menu_class' 	=> 'nav navbar-nav',
						'container'		=> false,
						'fallback_cb' => false,
						'walker' => new imprint_bootstrap_navwalker()
					)); ?>
					<ul class="nav navbar-nav navbar-right navbar-social">
						<?php imprint_social('li'); ?>
						<li class="nav-search">
							<a href="#search">
								<i class="fa fa-search"></i>
							</a>
						</li>

					</ul>
				<?php endif; ?>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	<?php }

	// if you want to override the footer, put your footer markup here.
	function footer_override() {

	}
	function imprint_init_actions() {
		global $ImprintInit;
		// add removals here if you want
		remove_action('imprint_top_nav', array($ImprintInit, 'imprint_top_navigation'));
		add_action('imprint_top_nav', array($this, 'ic_top_nav'), 10);
	}

	// Register Scripts
	function custom_scripts() {

	}
	// Register Styles
	function custom_styles() {
		// This is a good example of enqueuing google fonts...
//		wp_enqueue_style( 'GoogleFonts', 'http://fonts.googleapis.com/css?family=Droid+Serif:400,700,700italic,400italic|Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700', false, '1.0', 'screen' );
		wp_enqueue_style( 'IC_Styles', get_stylesheet_directory_uri().'/assets/css/themestyles.css', false, '1.0', 'screen' ); // this is where you will put your stylesheet
	}
}
$Imprint_Child_Init = new Imprint_Child_Init();
